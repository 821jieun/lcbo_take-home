import { throwError as observableThrowError, Observable} from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

//injectable decorator(only in a service)
//tells angular that this service itself might have dependencies
//without it, this would just be a typescript class
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  private _url: string = 'http://lcboapi.com/products?access_key=MDplZGM1YzY0Mi1iNTA4LTExZTgtYTI4OC03YjUyOGM1OWQ1ZTE6UkVvODhERHJTa3QzQk04ZlBIMlpUcjVSTHNvNUplTEFEc3I4';

  getBeverages() {
    return this.http.get(this._url)
                    .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || "server error")
  }

}
