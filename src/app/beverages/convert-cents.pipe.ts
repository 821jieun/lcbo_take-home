import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'convertCents'})
export class ConvertCentsPipe implements PipeTransform {
  transform(value: number): number {
    return value / 100;
  }
}
