import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
//rxjs will hold the data returned from the api
import { Observable } from 'rxjs';

@Component({
  selector: 'app-beverages',
  templateUrl: './beverages.component.html',
  styleUrls: ['./beverages.component.scss']
})
export class BeveragesComponent implements OnInit {

  //property for holding returned data from api
  beverages$: Object;

  public errorMsg;

  constructor(private data: DataService) { }

  //ngOnInit is a lifecycle hook
  //that will execute when this component has intialized
  ngOnInit() {
  //subscribing to the observable
    this.data.getBeverages()
        .subscribe( data => this.beverages$ = data,
                    error => this.errorMsg = error);
  }
}
