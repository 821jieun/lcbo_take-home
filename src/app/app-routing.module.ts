import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BeveragesComponent } from './beverages/beverages.component';

const routes: Routes = [
  {
    path: '',
    component: BeveragesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
